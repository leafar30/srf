package com.sf.srf.gw.auth;

import com.sf.srf.dto.AuthenticationResponseDto;
import java.time.Duration;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authenticationService;

    @PostMapping("/authentication")
    public Mono<ResponseEntity<AuthenticationResponseDto>> auth() {
        return authenticationService.authenticate()
                                    .map(authResponseDto -> ResponseEntity.ok()
                                                                          .header(HttpHeaders.SET_COOKIE,
                                                                                  ResponseCookie.from(HttpHeaders.AUTHORIZATION, authResponseDto.getToken())
                                                                                                .maxAge(Duration.between(Instant.now(),
                                                                                                                         Instant.ofEpochMilli(authResponseDto.getExpires())))
                                                                                                .build()
                                                                                                .toString())
                                                                          .body(authResponseDto))
                                    .onErrorResume(AuthFailedException.class, e -> Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()));
    }
}
