package com.sf.srf.gw.model;

import java.time.Duration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public class UserRepository {

    // here we store whole user by token
    private final ReactiveRedisTemplate<String, User> userByToken;

    public UserRepository(ReactiveRedisConnectionFactory factory) {
        var keySerializer = new StringRedisSerializer();
        userByToken = createTemplateForUserByToken(keySerializer, factory);
    }

    private ReactiveRedisTemplate<String, User> createTemplateForUserByToken(StringRedisSerializer keySerializer, ReactiveRedisConnectionFactory factory) {
        var valueSerializer = new Jackson2JsonRedisSerializer<>(User.class);
        var context = RedisSerializationContext.<String, User>newSerializationContext(keySerializer).value(valueSerializer).build();
        return new ReactiveRedisTemplate<>(factory, context);
    }

    public Mono<User> findUserByAuthToken(String token) {
        return userByToken.opsForValue().get(token);
    }

    public Mono<Boolean> saveUser(String token, User user, Duration ttl) {
        return userByToken.opsForValue().set(token, user, ttl);
    }


}
