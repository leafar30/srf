package com.sf.srf.gw.auth;

import com.sf.srf.gw.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class AuthFilter implements GatewayFilter {

    public static final String SRF_USER = "srf-userId";

    private final AuthService authService;


    private Mono<String> getToken(ServerWebExchange exchange) {
        var request = exchange.getRequest();
        return Mono.justOrEmpty(request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION));
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return getToken(exchange).flatMap(authService::findUserByAuthToken).switchIfEmpty(Mono.<User>defer(() -> {
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().writeWith(Mono.empty()).then(Mono.empty());
        })).flatMap(user -> {
            var request = exchange.getRequest().mutate().headers(headers -> {
                headers.set(SRF_USER, user.getId().toString());
            }).build();
            return chain.filter(exchange.mutate().request(request).build());
        });
    }
}