package com.sf.srf.gw.config;

import com.sf.srf.gw.auth.AuthFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfig {

    private static final String URL01 = "https://www.expedia.com/";
    private static final String URL02 = "https://www.skyscanner.de/";
    private static final String INFO = "http://localhost:8080/";


    private final AuthFilter authFilter;

    public RouteConfig(AuthFilter authFilter) {
        this.authFilter = authFilter;
    }

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                      .route(r -> r.path("/xp/**").filters(f -> f.rewritePath("/xp", "/")).uri(URL01))
                      .route(r -> r.path("/sk/**").filters(f -> f.rewritePath("/sk", "/")).uri(URL02))
                      .route(r -> r.path("/skfd/**").filters(f -> f.filter(authFilter).rewritePath("/skfd", "/srf/info/get")).uri(INFO))
                      .build();
    }
}
