package com.sf.srf.gw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrfGwApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrfGwApplication.class, args);
    }

}

