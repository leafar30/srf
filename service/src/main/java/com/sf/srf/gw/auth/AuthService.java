package com.sf.srf.gw.auth;

import com.sf.srf.dto.AuthenticationResponseDto;
import com.sf.srf.gw.model.User;
import com.sf.srf.gw.model.UserRepository;
import java.time.Clock;
import java.time.Duration;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository repository;
    private final Clock clock;

    public Mono<User> findUserByAuthToken(String token) {
        return repository.findUserByAuthToken(token);
    }

    public Mono<AuthenticationResponseDto> authenticate() {
        return this.createSession();
    }

    private Mono<AuthenticationResponseDto> createSession() {
        final UUID token = UUID.randomUUID();
        final User user = new User(UUID.randomUUID(), "RAFA");
        final Duration ttl = Duration.ofHours(1);

        final AuthenticationResponseDto authResponseDto = new AuthenticationResponseDto(token.toString(),
                                                                                        clock.instant().plus(ttl).toEpochMilli(),
                                                                                        user.getUsername());
        return repository.saveUser(token.toString(), user, ttl).then(Mono.just(authResponseDto));
    }
}