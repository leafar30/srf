package com.sf.srf.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@EqualsAndHashCode
@RequiredArgsConstructor
@Getter
public class AuthenticationResponseDto {

    private final String token;
    private final long expires;
    private final String user;
}
