FROM openjdk:11

COPY service/target/service-1.0.0.jar /opt/srf.jar

EXPOSE 8081

ENTRYPOINT run-jar /opt/srf.jar
